package edu.duke.ks713.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
/**This class is about the construction of rectangleship */
public class RectangleShip<T> extends BasicShip<T> {
    private final String name;
    HashMap<Integer,Coordinate> map;
    T hit_info;
    /**Get all the coordinates belongs to the ship
     * @param upperLeft the coordinate in the upper left corner of the ship
     * @param width the width of the ship
     * @param height the height of the ship
     * @return  a set contains all the coordinates belong to the ship*/
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        int up_leftx=upperLeft.getRow();
        int up_lefty=upperLeft.getColumn();
        ArrayList<Integer> x=new ArrayList<>();
        ArrayList<Integer> y=new ArrayList<>();
        HashSet<Coordinate> cor_set=new HashSet<>();
        x.add(up_leftx);
        int x_start=up_leftx;
        for(int i=1;i<=height-1;i++){
            x_start+=1;
            x.add(x_start);
        }
        y.add(up_lefty);
        int y_start=up_lefty;
        for(int j=1;j<=width-1;j++){
            y_start+=1;
            y.add(y_start);
        }
        for(int p=0;p<x.size();p++){
            for(int q=0;q<y.size();q++){
                Coordinate coordinate=new Coordinate(x.get(p),y.get(q));
                cor_set.add(coordinate);
            }
        }
       return cor_set;
    }
    /**Constructor for RectangleShip
     * @param name the name of the rectangleship differed by type
     * @param upperLeft the uppleft coordinate of the rectangleship
     * @param width   the width of the ship
     * @param height the height of the ship
     * @param data the data to display when this ship not hit
     * @param onHit the data to display when this ship hit.*/
    public RectangleShip(String name,Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name,upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null,data));
        this.hit_info=data;
    }
    /**The constructor of a rectangelship
     * @param upperLeft the upperLeft coordinate of the ship
     * @param data the data to display when this ship not hit
     * @param onHit the data to display when ship hit at specific  coordinate*/
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship",upperLeft, 1, 1, data, onHit);
    }
    /**The constructor of the rectangelship
     * @param name the name of the rectangeship differed by type.
     * @param upperLeft  the upperLeft coordinate of the ship
     * @param width  the width of the ship
     * @param height the height of the ship
     * @param shipDisplayInfo what to display at each location of my own ship
     * @param enemyInfo what to display at each location of my enemy's ship*/
    public RectangleShip(String name,Coordinate upperLeft, int width, int height,ShipDisplayInfo<T> shipDisplayInfo,ShipDisplayInfo<T> enemyInfo) {
        super(makeCoords(upperLeft, width, height),shipDisplayInfo,enemyInfo);
        this.name=name;
        char or='V';
        if(width>height){
            or='H';
        }
        this.map=new HashMap<>();
        HashSet<Coordinate> cors=makeCoords(upperLeft, width, height);
        make_order_cor(or,upperLeft,cors);
    }
   /**Get the name of this ship
    * @return the name of this ship*/

    @Override
    public String getName() {
        return name;
    }
    /** return the display info for hit
     * @return hit info*/
    @Override
    public T get_hit_info() {
        return hit_info;
    }
    /**Make the coordinates in order
     * @param or  is the orientation of the ship
     * @param upperLeft is the upperLeft coordinate
     * @param set is the set of coordinates for the ship*/
    public void make_order_cor(char or, Coordinate upperLeft,HashSet<Coordinate> set){
        if(name.equals("Submarine")){
            int row=upperLeft.getRow();
            int column= upperLeft.getColumn();
            if(or=='v'||or=='V'){
                map.put(0,upperLeft);
                map.put(1,new Coordinate(row+1,column));
            }else if(or=='h'||or=='H'){
                map.put(0,upperLeft);
                map.put(1,new Coordinate(row,column+1));
            }
        }else if(name.equals("Destroyer")){
            int row=upperLeft.getRow();
            int column= upperLeft.getColumn();
            if(or=='v'||or=='V'){
                map.put(0,upperLeft);
                map.put(1,new Coordinate(row+1,column));
                map.put(2,new Coordinate(row+2,column));
            }else if(or=='h'||or=='H'){
                map.put(0,upperLeft);
                map.put(1,new Coordinate(row,column+1));
                map.put(2,new Coordinate(row,column+2));
            }
        }
    }
    /**Return the coordinates in order
     * @return the coordinates in order*/
    @Override
    public HashMap<Integer, Coordinate> get_ship_position_inorder() {
        return map;
    }
}
