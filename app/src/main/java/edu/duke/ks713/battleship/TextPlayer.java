package edu.duke.ks713.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;

import static java.lang.System.exit;
/**THis class handles the play round for one player*/
public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    final HashMap<String, Function<BCPlacement, Ship<Character>>> shipCreationFns2;
    final HashMap<String,Integer> actionCount;
    final boolean isComputer;
    ArrayList<String> combinations;
    ArrayList<Coordinate> cordinatesS;
    HashMap<Ship<Character>,Placement> shipPlacementMap;
    HashMap<Ship<Character>,BCPlacement> shipBCPlacementMap;
    /**Construct a player object for this battleship game
     * @param name the name of the player
     * @param theBoard  the borad belongs to the player
     * @param  inputReader the inputReader of the whole system
     * @param out The output stream
     * @param shipFactory the class help us to make ship.
     * @param isComputer whethr this player is a computer player*/
    public TextPlayer(String name,Board<Character>theBoard,BufferedReader inputReader,PrintStream out,AbstractShipFactory<Character> shipFactory,boolean isComputer){
        this.name=name;
        this.theBoard=theBoard;
        this.inputReader=inputReader;
        this.out=out;
        this.shipFactory=shipFactory;
        this.shipsToPlace=new ArrayList<>();
        this.shipCreationFns=new HashMap<>();
        this.shipCreationFns2=new HashMap<>();
        this.actionCount=new HashMap<>();
        this.actionCount.put("M",3);
        this.actionCount.put("S",3);
        this.setupShipCreationMap();
        this.setupShipCreationList();
        this.isComputer=isComputer;
        this.combinations=new ArrayList<>();
        this.cordinatesS=new ArrayList<>();
        this.shipPlacementMap=new HashMap<>();
        this.shipBCPlacementMap=new HashMap<>();
    }
    /**Find If coordinates in old ship was hit, if you, the information will be placed correspondingly in ship2
     * @param ship1 is the old ship
     * @param ship2 is the new ship*/
    public void get_relative_position(Ship<Character> ship1,Ship<Character> ship2){
      HashMap<Integer,Coordinate> map1=ship1.get_ship_position_inorder();
      HashMap<Integer,Coordinate> map2=ship2.get_ship_position_inorder();
      for(int x:map1.keySet()){
          if(ship1.wasHitAt(map1.get(x))){
              ship2.recordHitAt(map2.get(x),true);
          }
      }
    }
    /**This method reads the coordinate from the user input
     * @param prompt the prompt to help user input
     * @return return the coordinate constructed from user input.
     * @throws EOFException when the reader meets EOF
     * @throws IOException when IO error happens*/
    public Coordinate readCoordinate(String prompt) throws IOException{
        prompt=prompt.replaceAll("\r","");
        out.println(prompt);
        String s=inputReader.readLine();
        if(s==null){
            throw new EOFException();
        }
        return new Coordinate(s);
    }
    /**This method reads the BCPlacement from the user input
     * @param prompt the prompt to help user input
     * @return return the coordinate constructed from user input.
     * @throws EOFException when the reader meets EOF
     * @throws IOException when IO error happens*/
    public BCPlacement readBCPlacement(String prompt) throws IOException{
        prompt=prompt.replaceAll("\r","");
        out.println(prompt);
        String s = inputReader.readLine();
        //  System.out.println(s.charAt(s.length()-1)=='\u001a');
        if(s==null){
            throw new EOFException();
        }
        s=s.replaceAll("\r","");
        return new BCPlacement(s);
    }
    /**This method reads the placement from the user input
     * @param prompt the prompt to help user input
     * @return return the placement constructed from user input.
     * @throws EOFException when the reader meets EOF
     * @throws IOException when IO error happens*/
    public Placement readPlacement(String prompt) throws IOException {
        prompt=prompt.replaceAll("\r","");
        out.println(prompt);
        String s = inputReader.readLine();
     //  System.out.println(s.charAt(s.length()-1)=='\u001a');
        if(s==null){
            throw new EOFException();
        }
        s=s.replaceAll("\r","");
        return new Placement(s);
    }
    /**This method do the interactive placement phase with the user
     * @param isComputer  whether this player is a computer player*/
    public void doPlacementPhase(boolean isComputer) throws IOException{
        BoardTextView btv1=new BoardTextView(theBoard);
        if(!isComputer){
            out.println("Player "+name+"'s"+" turn");
            out.println(btv1.displayMyOwnBoard());
        }
        String s="Player "+name+": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2 \n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n";
        if(!isComputer){
            out.println(s);
        }
        for(int i=0;i<shipsToPlace.size();i++){
            String shipName= shipsToPlace.get(i);
            if(shipName.equals("Battleship")||shipName.equals("Carrier")){
                Function<BCPlacement, Ship<Character>> func=shipCreationFns2.get(shipName);
                String msg="Player " + name + " where do you want to place a " + shipName + "?";
                this.doOnePlacement(null,shipName,msg,null,func,false,isComputer);
            }else{
                Function<Placement, Ship<Character>> func=shipCreationFns.get(shipName);
                String msg="Player " + name + " where do you want to place a " + shipName + "?";
                this.doOnePlacement(null,shipName,msg,func,null,false,isComputer);
            }
        }

    }
    /**One step of the placement plase
     * @param org  the old ship
     * @param shipName the name of the ship
     * @param msg  the msg to display for placement
     * @param createFn the ship making function of Submarine and Destroyer
     * @param func2  the ship making function of Battleship and Carrier
     * @param isComputer  whether the player is a computer player
     * @param need_exit  whether the method need exit whenver error happens
     * @return true if placement succeed false otherwise*/
    public boolean doOnePlacement(Ship<Character> org,String shipName,String msg, Function<Placement, Ship<Character>> createFn,Function<BCPlacement,Ship<Character>>func2,boolean need_exit,boolean isComputer) throws IOException {
        //System.out.println(isComputer+" hellollo");
        while(!isComputer){
            try{
                //Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                while(true){

                    if(shipName.equals("Battleship")||shipName.equals("Carrier")){
                        BCPlacement p=readBCPlacement(msg);
                        Ship<Character> s = func2.apply(p);
                        if(theBoard.tryAddShip(s)!=null){
                            out.println(theBoard.tryAddShip(s));
                            if(need_exit){
                                return false;
                            }
                        }else{
                            shipBCPlacementMap.put(s,p);
                            if(org!=null){
                                get_relative_position(org,s);
                            }
                            break;
                        }
                    }else{
                        Placement p = readPlacement(msg);
                        Ship<Character> s = createFn.apply(p);
                        if(theBoard.tryAddShip(s)!=null){
                            out.println(theBoard.tryAddShip(s));
                            if(need_exit){
                                return false;
                            }
                        }else{
                            shipPlacementMap.put(s,p);
                            if(org!=null){
                                get_relative_position(org,s);
                            }
                            break;
                        }
                    }
                }
                BoardTextView view=new BoardTextView(theBoard);
                out.println("Current ocean:");
                out.print(view.displayMyOwnBoard());
                break;
            }catch(IllegalArgumentException e){
                out.println("The placement is invalid! It does not have a correct format!");
                if(need_exit){
                    return false;
                }
            }
        }
        if(isComputer){
           if(combinations.size()>=0){
               for(char a='A';a<='Z';a++){
                   boolean need_break=true;
                   for(int x=0;x<=9;x++){
                       String row=Character.toString(a);
                       String column=Integer.toString(x);
                       if(shipName.equals("Battleship")||shipName.equals("Carrier")){
                           combinations.add(row+column+"U");
                           combinations.add(row+column+"R");
                           combinations.add(row+column+"D");
                           combinations.add(row+column+"L");
                       }else{
                           combinations.add(row+column+"V");
                           combinations.add(row+column+"H");
                       }
                   }
               }
           }
           Collections.shuffle(combinations);
           if(shipName.equals("Battleship")||shipName.contains("Carrier")){
               int index=0;
              while(true){
                  try{
                      BCPlacement bc=new BCPlacement(combinations.get(index));
                      Ship<Character> s = func2.apply(bc);
                      boolean satisfy=true;
                      if(theBoard.tryAddShip(s)!=null){
                          satisfy=false;
                      }
                      if(satisfy){
                          shipBCPlacementMap.put(s,bc);
                          combinations.remove(index);
                          break;
                      }
                  }catch(Exception e){

                  }
                  index+=1;
                  if(index> combinations.size()-1){
                      break;
                  }
              }
           }else{
               int index=0;
              while(true){
                  Placement pl=new Placement(combinations.get(index));
                  Ship<Character> s= createFn.apply(pl);
                  boolean satisfy=true;
                  if(theBoard.tryAddShip(s)!=null){
                      satisfy=false;
                  }
                  if(satisfy){
                      shipPlacementMap.put(s,pl);
                      combinations.remove(index);
                      break;
                  }
                  index+=1;
                  if(index> combinations.size()-1){
                      break;
                  }
              }
           }
        }

        return true;
    }
    /**Set up ship creation functions*/
    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns2.put("Battleship", (q) -> shipFactory.makeBattleship(q));
        shipCreationFns2.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }
    /**Initialize ship list*/
    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(2, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(3, "Carrier"));
    }
    /**Check if this player has lost
     * @return the boolean variable state whether the user has lost*/
    public boolean check_lost(){
        return theBoard.allSunk();
    }
    /**sonar scan function*/
    public void sonar_scan() throws IOException{
        ArrayList<Integer> arr=new ArrayList<>();
        while(true){
            try{
                Coordinate center=readCoordinate("Please input the center coordinate for sonar scan ");
                int row=center.getRow();
                for(int i=-3;i<=3;i++){
                    arr.add(row+i);
                }
                HashSet<Coordinate> cors=new HashSet<>();
                for(int j=0;j<7;j++){
                    cors.add(new Coordinate(arr.get(j),center.getColumn()));
                    if(j==1||j==5){
                        cors.add(new Coordinate(arr.get(j),center.getColumn()-1));
                        cors.add(new Coordinate(arr.get(j),center.getColumn()+11));
                    }else if(j==2||j==4){
                        int start=center.getColumn()-2;
                        int end=center.getColumn()+2;
                        for(int k=start;k<=end;k++){
                            if(k!=center.getColumn()){
                                cors.add(new Coordinate(arr.get(j),k));
                            }
                        }
                    }else if(j==3){
                        int start=center.getColumn()-3;
                        int end=center.getColumn()+3;
                        for(int k=start;k<=end;k++){
                            if(k!=center.getColumn()){
                                cors.add(new Coordinate(arr.get(j),k));
                            }
                        }
                    }
                }
                HashMap<String,Integer> map=theBoard.sonar_scan_helper(cors);
                for(String s1:map.keySet()){
                    out.println(s1+" occupy "+map.get(s1)+" squares");
                }
                break;
            }catch(Exception e){
                out.println("Your location string is not valid, please input in the format like A0");
            }
        }
        actionCount.put("S", actionCount.get("S")-1);
    }
    /** move ship main function
     * @param enemy is the enemy player*/
    public void move_ship(TextPlayer enemy) throws IOException{
      out.println("You choose to move your ship!! Refer to your ocean diplayed to you! Please input a valid coordinate in string");
      out.println("This coordinate should belongs to the ship you want to move.");
        boolean a=true;
          try{
              Coordinate cor=readCoordinate("Please input the coordinate");
              if(this.theBoard.whatIsAt(cor,true)==null){
                  a=false;
                  out.println("Your input is invalid!! Your coordinate does not belong to any ship in your own board!");
              }else{
                  Ship<Character> ship= theBoard.find_ship(cor);
                  String shipName=ship.getName();
                  boolean status=true;
                  if(shipName.equals("Battleship")||shipName.equals("Carrier")){
                      Function<BCPlacement,Ship<Character>> func=shipCreationFns2.get(shipName);
                      String msg="Where do want to move this "+shipName+"?";
                      theBoard.remove_ship(ship);
                      status=doOnePlacement(ship,shipName,msg,null,func,true,false);
                  }else{
                      Function<Placement,Ship<Character>> func=shipCreationFns.get(shipName);
                      String msg="Where do want to move this "+shipName+"?";
                      theBoard.remove_ship(ship);
                      status=doOnePlacement(ship,shipName,msg,func,null,true,false);
                  }
                  a=status;
                  if(a){
                      theBoard.hit_record(ship.get_hit_coordinates(),ship.get_hit_info());
                  }else{
                      theBoard.tryAddShip(ship);
                  }
              }
          }catch(Exception e){
              a=false;
              out.println("Your input is an invalid coordinate string!");
          }
          if(!a){
              action_direction(enemy,false);
          }
        actionCount.put("M", actionCount.get("M")-1);
    }
    /**The direction of all actions
     *@param enemy  the enemy user
     * @param isComputer  whether the player is a computer*/
    public void action_direction(TextPlayer enemy,boolean isComputer) throws IOException{
        String minfo=" M Move a ship to another square ("+actionCount.get("M")+"remaining)"+"\n";
        if(actionCount.get("M")==0){
            minfo="";
        }
        String sinfo=" S Sonar scan ("+actionCount.get("S")+"remaining)"+"\n";
        if(actionCount.get("S")==0){
            sinfo="";
        }
        String aInfo="Player "+name+", what would you like to do? You must input F";
        if(actionCount.get("S")!=0){
            aInfo+="or S";
        }
        if(actionCount.get("M")!=0){
            aInfo+="or M";
        }
        String actionInfo="Possible actions for Player "+name+":\n" +
                "\n" +
                " F Fire at a square\n" +
                minfo +
                sinfo+
                "\n" +
                aInfo;
        if(!isComputer){
            out.println(actionInfo);
        }
        boolean af=true;
        boolean aM=true;
        boolean aS=true;
        while(!isComputer){
            String actionType=inputReader.readLine();
            af=actionType.equals("F");
            aM=actionType.equals("M");
            aS=actionType.equals("S");
            if(af||aM||aS){
                break;
            }else{
                out.println("Please input F or M or S!");
            }
        }
        //Coordinate prompt Check.
        while(af){
            try{
                if(!isComputer){
                    String msg="Please input the coordinate you want to fire at:";
                    Coordinate fireCor=readCoordinate(msg);
                    out.println(enemy.theBoard.fireAt(name,fireCor));
                }else{
                    if(cordinatesS.size()>=0){
                        for(char a='A';a<='T';a++){
                            boolean need_break=true;
                            for(int x=0;x<=9;x++){
                               cordinatesS.add(new Coordinate(Character.toString(a)+Integer.toString(x)));
                            }
                        }
                    }
                    Collections.shuffle(cordinatesS);
                    out.println(enemy.theBoard.fireAt(name,cordinatesS.get(0)));
                    cordinatesS.remove(0);
                }
                break;
            }catch(IllegalArgumentException e){
                if(!isComputer){
                    out.println("That coordinate is invalid: it does not have the correct format.");
                }
            }
        }
        if(aM&&!isComputer){
            move_ship(enemy);
        }else if(aS&&!isComputer){
            sonar_scan();
        }
    }
    /**The method for the user to play one turn
     * @param enemy the enemy player
     * @param isComputer whether the player is a computer
     * @throws IOException when calling other method possibly*/
    public void playOneTurn(TextPlayer enemy,boolean isComputer) throws IOException{
       BoardTextView myView=new BoardTextView(this.theBoard);
       BoardTextView enemyView=new BoardTextView(enemy.theBoard);
       if(!isComputer){
           out.println("Player "+name+"'s"+" turn");
       }
       String myHeader="Your ocean";
       String enemyHeader="Player "+enemy.name+"'s"+"ocean";
       if(!isComputer){
           out.println(myView.displayMyBoardWithEnemyNextToIt(enemyView,myHeader,enemyHeader));
       }
       action_direction(enemy,isComputer);
    }
}
