package edu.duke.ks713.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**This interface have important functions of a board*/
public interface Board<T>{
    /**Get the width of the board
     * @return return the width of the board*/
    public int getWidth();
    /**
     * get the height of the BattleShipBoard
     * @return return the height of the BattleShipBoard
     */
    public int getHeight();
    /** get the display Info at specific location
     * @param where is the coordinate needs to be checked whether is in the board or not
     * @param self is a boolean variable indicate whether the board is mine or enemy's
     * @return T if the coordinate belongs to the board null otherwise*/
    public T whatIsAt(Coordinate where,boolean self);
    /** add the ship to the board if it follows the placement rules
     * @param toAdd is the ship which is about to be added to the board
     * @return null if add succeed string if not succeed with specific message*/
    public String tryAddShip(Ship<T> toAdd);
    /**Hit the ship in the board at the specific location
     * @param c is th hit location
     * @return return "You hit a ship's name" if hit succeed or"You missed" If hit fail.*/
    public String fireAt(String name,Coordinate c);
    /**get the display info at specific position for self board
     * @param where is the coordinate to get display info
     * @return return the display info at this position*/
    public T whatIsAtForSelf(Coordinate where);
    /**get the display info at specific position for enemy's board
     * @param where is the coordinate to get display info
     * @return return the display info at this position*/
    public T whatIsAtForEnemy(Coordinate where);
    /**Check if all the ships that belongs to the board is sunk
     * @return a boolean variable state whether all the ships belong to the board is sunk.*/
    public boolean allSunk();
    /**find the ship that oocupy the coordinate
     * @param where the location
     * @return the ship that occupy where*/
    public Ship<T> find_ship(Coordinate where);
    /**record hit location seperately*
     * @param cors the hit locations
     */
    public void hit_record(HashSet<Coordinate > cors,T info);
    /** remove the ship from the board
     * @param ship  is the ship to be removed*/
    public void remove_ship(Ship<T> ship);
    /**sonar scan helper for sonar scan operation
     * @param cor is the coordinates for sonar scan*/
    public HashMap<String,Integer> sonar_scan_helper(HashSet<Coordinate> cor);
}
