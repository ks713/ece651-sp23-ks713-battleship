package edu.duke.ks713.battleship;

import java.util.HashMap;
import java.util.HashSet;
/**This class handle the special Battleship and Carrier*/
public class BCShip<T> extends BasicShip<T>{
    T hit_info;
    HashMap<Integer,Coordinate> map;
    /**make the coordinates in order
     * @param or orientation
     * @param upperLeft  the upperleft coordinate
     * @param cords  is the coordinates*/
    public void make_battle_cord_order(char or,Coordinate upperLeft,HashSet<Coordinate> cords){
        if(or=='U'||or=='u'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row,column+1));
            map.put(1,new Coordinate(row+1,column));
            map.put(2,new Coordinate(row+1,column+1));
            map.put(3,new Coordinate(row+1,column+2));
        }else if(or=='R'||or=='r'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row+1,column+1));
            map.put(1,new Coordinate(row,column));
            map.put(2,new Coordinate(row+1,column));
            map.put(3,new Coordinate(row+2,column));
        }else if(or=='D'||or=='d'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row+1,column+1));
            map.put(1,new Coordinate(row,column+2));
            map.put(2,new Coordinate(row,column+1));
            map.put(3,new Coordinate(row,column));
        }else if(or=='L'||or=='l'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row+1,column));
            map.put(1,new Coordinate(row+2,column+1));
            map.put(2,new Coordinate(row+1,column+1));
            map.put(3,new Coordinate(row,column+1));
        }
    }
    /**make the coordinates in order
     * @param or orientation
     * @param upperLeft  the upperleft coordinate
     * @param cords  is the coordinates*/
    public void make_carrier_cords_order(char or,Coordinate upperLeft,HashSet<Coordinate> cords){
        if(or=='U'||or=='u'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row,column));
            map.put(1,new Coordinate(row+1,column));
            map.put(2,new Coordinate(row+2,column));
            map.put(3,new Coordinate(row+3,column));
            map.put(4,new Coordinate(row+2,column+1));
            map.put(5,new Coordinate(row+3,column+1));
            map.put(6,new Coordinate(row+4,column+1));
        }else if(or=='R'||or=='r'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row,column+4));
            map.put(1,new Coordinate(row,column+3));
            map.put(2,new Coordinate(row,column+2));
            map.put(3,new Coordinate(row,column+1));
            map.put(4,new Coordinate(row+1,column+2));
            map.put(5,new Coordinate(row+1,column+1));
            map.put(6,new Coordinate(row+1,column));
        }else if(or=='D'||or=='d'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row+4,column+1));
            map.put(1,new Coordinate(row+3,column+1));
            map.put(2,new Coordinate(row+2,column+1));
            map.put(3,new Coordinate(row+1,column+1));
            map.put(4,new Coordinate(row+2,column));
            map.put(5,new Coordinate(row+1,column));
            map.put(6,new Coordinate(row,column));
        }else if(or=='L'||or=='l'){
            int row= upperLeft.getRow();
            int column= upperLeft.getColumn();
            map.put(0,new Coordinate(row+1,column));
            map.put(1,new Coordinate(row+1,column+1));
            map.put(2,new Coordinate(row+1,column+2));
            map.put(3,new Coordinate(row+1,column+3));
            map.put(4,new Coordinate(row,column+2));
            map.put(5,new Coordinate(row,column+3));
            map.put(6,new Coordinate(row,column+4));
        }
    }
    /**make the coordinates for battleship
     * @param orientation is orientation
     * @param upperLeft  upperLeft coordinate
     * @return coordinates*/
    static HashSet<Coordinate> makeBattleShipCoords(char orientation,Coordinate upperLeft){
        if(orientation=='U'||orientation=='u'){
            HashSet<Coordinate>  cors=new HashSet<>();
            cors.add(new Coordinate(upperLeft.getRow(), (upperLeft.getColumn())+1));
            cors.add(new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn() ));
            cors.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
            cors.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+2));
            return cors;
        }else if(orientation=='R'||orientation=='r'){
            HashSet<Coordinate>  cors=new HashSet<>();
            cors.add(upperLeft);
            cors.add(new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn() ));
            cors.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
            cors.add(new Coordinate(upperLeft.getRow()+2,upperLeft.getColumn()));
            return cors;
        }else if(orientation=='D'||orientation=='d'){
            HashSet<Coordinate>  cors=new HashSet<>();
            cors.add(upperLeft);
            cors.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()+1 ));
            cors.add(new Coordinate(upperLeft.getRow(),upperLeft.getColumn()+2));
            cors.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
            return cors;
        }else if(orientation=='l'||orientation=='L'){
            HashSet<Coordinate>  cors=new HashSet<>();
            cors.add(new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn() ));
            cors.add(new Coordinate(upperLeft.getRow()+2,upperLeft.getColumn()+1));
            cors.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
            cors.add(new Coordinate(upperLeft.getRow(),upperLeft.getColumn()+1));
            return cors;
        }
        return new HashSet<>();
    }
    /**make the coordinates for Carrier
     * @param orientation is orientation
     * @param upperLeft  upperLeft coordinate
     * @return coordinates*/
    static HashSet<Coordinate> makeCarrierCoords(char orientation,Coordinate upperLeft){
        if(orientation=='U'||orientation=='u'){
             HashSet<Coordinate> set=new HashSet<>();
             int c1= upperLeft.getRow();
             for(int i=0;i<4;i++){
                set.add(new Coordinate(c1, upperLeft.getColumn()));
                c1+=1;
             }
             int c2= upperLeft.getRow()+2;
             for(int j=0;j<3;j++){
                 set.add(new Coordinate(c2, upperLeft.getColumn()+1));
                 c2+=1;
             }
             return set;
        }else if(orientation=='R'||orientation=='r'){
            HashSet<Coordinate> set=new HashSet<>();
            int c1= upperLeft.getColumn()+1;
            for(int i=0;i<4;i++){
                set.add(new Coordinate(upperLeft.getRow(), c1));
                c1+=1;
            }
            int c2= upperLeft.getColumn();
            for(int j=0;j<3;j++){
                set.add(new Coordinate(upperLeft.getRow()+1, c2));
                c2+=1;
            }
            return set;

        }else if(orientation=='D'||orientation=='d'){
            HashSet<Coordinate> set=new HashSet<>();
            int c1= upperLeft.getRow()+1;
            for(int i=0;i<4;i++){
                set.add(new Coordinate(c1, (upperLeft.getColumn())+1));
                c1+=1;
            }
            int c2= upperLeft.getRow();
            for(int j=0;j<3;j++){
                set.add(new Coordinate(c2, upperLeft.getColumn()));
                c2+=1;
            }
            return set;

        }else if(orientation=='l'||orientation=='L'){
            HashSet<Coordinate> set=new HashSet<>();
            int c1= upperLeft.getColumn()+2;
            for(int i=0;i<3;i++){
                set.add(new Coordinate(upperLeft.getRow(), c1));
                c1+=1;
            }
            int c2= upperLeft.getColumn();
            for(int j=0;j<4;j++){
                set.add(new Coordinate(upperLeft.getRow()+1, c2));
                c2+=1;
            }
            return set;
        }
        return new HashSet<>();
    }
    /**make the coordinates
     * @param name is the name of the ship
     * @param orientation is orientation
     * @param upperLeft  upperLeft coordinate
     * @return coordinates*/
    static HashSet<Coordinate> makeCoords(String name,char orientation,Coordinate upperLeft) {
        if(name.equals("Battleship")){
            return makeBattleShipCoords(orientation,upperLeft);
        }else if(name.equals("Carrier")){
            return makeCarrierCoords(orientation, upperLeft);
        }
        return new HashSet<>();
    }
    final String name;
    /**getters for name
     * @return name*/
    @Override
    public String getName() {
        return name;
    }
    /**return hit coordinates
     * @return coordinates hit*/
    @Override
    public HashSet<Coordinate> get_hit_coordinates() {
        HashSet<Coordinate> hits=new HashSet<>();
        for(Coordinate cor:this.myPieces.keySet()){
            if(myPieces.get(cor)){
                hits.add(cor);
            }
        }
        return hits;
    }
    /**return hit info
     * @return hit info*/

    @Override
    public T get_hit_info() {
        return hit_info;
    }
    /**return the coordinates in order
     * @return the coordinates in order as map*/
    @Override
    public HashMap<Integer, Coordinate> get_ship_position_inorder() {
        return map;
    }
    /**construct a BCShip
     * @param name is the name of the ship
     * @param orientation  is the orientation of the ship
     * @param upperLeft is the upperLeft coordinate of the ship
     * @param data is the display data of the ship
     * @param onhit is the display info when ship hit*/
    public BCShip(String name,char orientation,Coordinate upperLeft,T data, T onhit){
        this(name,orientation,upperLeft,new SimpleShipDisplayInfo<T>(data,onhit),new SimpleShipDisplayInfo<T>(null,data));
        this.hit_info=data;
        this.map=new HashMap<>();
    }
    /**construct a BCShip
     * @param name is the name of the ship
     * @param orientation  is the orientation of the ship
     * @param upperLeft is the upperLeft coordinate of the ship
     * @param shipDisplayInfo is the display data of my ship
     * @param enemyInfo is the display info of enemy */
    public BCShip(String name,char orientation, Coordinate upperLeft,ShipDisplayInfo<T> shipDisplayInfo,ShipDisplayInfo<T> enemyInfo) {
        super(makeCoords(name,orientation,upperLeft),shipDisplayInfo,enemyInfo);
        HashSet<Coordinate> cords=makeCoords(name,orientation,upperLeft);
        this.map=new HashMap<>();
        if(name.equals("Battleship")){
            make_battle_cord_order(orientation,upperLeft,cords);
        }else if(name.equals("Carrier")){
            make_carrier_cords_order(orientation,upperLeft,cords);
        }
        this.name=name;
    }
}
