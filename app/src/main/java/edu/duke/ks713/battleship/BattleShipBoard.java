package edu.duke.ks713.battleship;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**This class is the description of board*/
public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    HashSet<Coordinate> enemyMisses;
    HashSet<Coordinate> hits;
    final T missInfo;
    T hit_info;
    /**
     * Constructs a BattleShipBoard with the specified width
     * and height and initialize all the corresponding parameters
     * @param new_width is the width of the newly constructed board.
     * @param new_height is the height of the newly constructed board.
     * @param missInfo the info to display when attacker miss an attack
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int new_width, int new_height,T missInfo){
        if (new_width<= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + new_width);
        }
        if (new_height<= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + new_height);
        }
        this.width = new_width;
        this.height = new_height;
        this.myShips=new ArrayList<>();
        this.placementChecker=new NoCollisionRuleChecker<>(new InBoundsRuleChecker<T>(null));
        this.enemyMisses=new HashSet<>();
        this.missInfo=missInfo;
        this.hits=new HashSet<>();
        this.hit_info=null;
    }
    /**
     * get the width of the BattleShipBoard
     * @return  return the width of the BattleShipBoard
     */
    public int getWidth(){
        return width;
    }
    /**
     * get the height of the BattleShipBoard
     * @return return the height of the BattleShipBoard
     */
    public int getHeight(){
        return height;
    }
    /** add the ship to the board if it follows the placement rules
     * @param toAdd is the ship which is about to be added to the board
     * @return null if add succeed string if not succeed with specific message*/
    public String tryAddShip(Ship<T> toAdd){
        if(this.placementChecker.checkPlacement(toAdd,this)==null){
            myShips.add(toAdd);
            return null;
        }
        return this.placementChecker.checkPlacement(toAdd,this);
    }
    /** get the display Info at specific location
     * @param where is the coordinate needs to be checked whether is in the board or not
     * @param self is a boolean variable indicate whether the board is mine or enemy's
     * @return T if the coordinate belongs to the board null otherwise*/
    public T whatIsAt(Coordinate where,boolean self) {
        if(!self){
            if(enemyMisses.contains(where)){
                return missInfo;
            }
        }
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                if(s.get_cors().size()>0){
                    if(s.get_cors().contains(where)){
                        if(!self){
                            return null;
                        }
                    }
                }
                return s.getDisplayInfoAt(where,self);
            }
        }
        if(hits.contains(where)&&!self){
            return hit_info;
        }
        return null;
    }
    /**get the display info at specific position for self board
     * @param where is the coordinate to get display info
     * @return return the display info at this position*/
    public T whatIsAtForSelf(Coordinate where){
        return whatIsAt(where,true);
    }
    /**get the display info at specific position for enemy's board
     * @param where is the coordinate to get display info
     * @return return the display info at this position*/
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }
    /**Hit the ship in the board at the specific location
     * @param c is th hit location
     * @return return "You hit a ship's name" if hit succeed or"You missed" If hit fail.*/
    public String fireAt(String name,Coordinate c){
        for(Ship<T> ship:myShips){
            if(ship.occupiesCoordinates(c)){
                ship.recordHitAt(c,false);
                return "Player "+name+"  hit a "+ship.getName()+"!";
            }
        }
        enemyMisses.add(c);
        return "Player "+name+" missed!";
    }
    /**Check if all the ships that belongs to the board is sunk
     * @return allSunk is a boolean variable state whether all the ships belong to the board is sunk.*/
    public boolean allSunk(){
        boolean allSunk=true;
        for(Ship<T> ship: myShips){
            if(!ship.isSunk()){
                allSunk=false;
                break;
            }
        }
        return allSunk;
    }
    /**return the ship oocupy the coordinate
     * @param where is the coordinate to find ship
     * @return null if no ship or ship*/
    @Override
    public Ship<T> find_ship(Coordinate where) {
        for(Ship<T> ship:myShips){
            if(ship.occupiesCoordinates(where)){
                return ship;
            }
        }
        return null;
    }
    /**record the hit  but removed points
     * @param cors is coordinates
     * @param info is the display info when hit*/
    @Override
    public void hit_record(HashSet<Coordinate> cors,T info) {
        hits.addAll(cors);
        this.hit_info=info;
    }
    /**remove the ship
     * @param ship is ship to be removed*/
    @Override
    public void remove_ship(Ship<T> ship) {
       if(myShips.contains(ship)){
           myShips.remove(ship);
       }
    }
    /**helper method for sonar scan,return the count for each ship for each sonar region
     * @param cor is the set of coordinates of sonar scan
     * @return a hashmap contains the results*/
    @Override
    public HashMap<String, Integer> sonar_scan_helper(HashSet<Coordinate> cor) {
        HashMap<String,Integer> scan_result=new HashMap<>();
        scan_result.put("Submarine",0);
        scan_result.put("Battleship",0);
        scan_result.put("Carrier",0);
        scan_result.put("Destroyer",0);
        Iterator<Coordinate> cor_iter=cor.iterator();
        while(cor_iter.hasNext()){
            Coordinate c=cor_iter.next();
            for(int i=0;i< myShips.size();i++){
                if(myShips.get(i).occupiesCoordinates(c)){
                    scan_result.put(myShips.get(i).getName(),scan_result.get(myShips.get(i).getName())+1);
                }
            }
        }
        return scan_result;
    }
}
