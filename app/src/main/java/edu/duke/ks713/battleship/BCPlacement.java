package edu.duke.ks713.battleship;
/**This class handles the special usage in placement for Battleship and Carrier*/
public class BCPlacement {
    private final Coordinate where;
    private final char orientation;
    /**Construct a special placement for Battleship and Carrier
     * @param where is the coordinate of the placement
     *@param orientation is the direction of the placement*/
    public BCPlacement(Coordinate where, char orientation){
        this.where=where;
        this.orientation=orientation;
    }
    /**
     * Constructs a BCPlacement with location string
     * @param descr is the location string, the combination of coordinates and the direction
     * @throws IllegalArgumentException if descr not meet the requirement
     */
    public BCPlacement(String descr){
        String descr2=descr.replaceAll(" ","");
            descr2=descr2.replaceAll("\n","");
            descr2=descr2.replaceAll("\r","");
        if(descr2.length()!=3){
            throw new IllegalArgumentException();
        }
        String first_part=descr2.substring(0,descr2.length()-1);
        Coordinate x=new Coordinate(first_part);
        this.where=x;
        char or=descr2.charAt(descr2.length()-1);
        boolean direction_judge=(or=='u'||or=='U')||(or=='l'||or=='L')||(or=='R'||or=='r')||(or=='D'||or=='d');
        if(!direction_judge){
            throw new IllegalArgumentException();
        }
        this.orientation=or;
    }
    /**The getter method of the coordinate where*
     * @return  the coordination of this BCPlacement
     */
    public Coordinate getWhere(){
        return where;
    }
    /**The getter method of the orientation
     * @return return the orinetation of this BCPlacement*/
    public char getOrientation(){
        return orientation;
    }
    /**The toString method of BCPlacement class, similar to the toString method of Coordinate class
     * @return the information of the object as string
     */
    @Override
    public String toString() {
        return "("+where.toString()+", " + orientation+")";
    }
    /**The hashcode method of the BCPlacement class
     * @return return the hashcode of the object*/
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
    /**The equals method of the BCPlacement class, similar to Coordinate class
     * @return whether the two object are equal*/
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            BCPlacement c = (BCPlacement) o;
            String thisOri=Character.toString(this.orientation);
            String cOri=Character.toString(c.orientation);
            return c.where.equals(((BCPlacement) this).where)&&(thisOri.equalsIgnoreCase(cOri));
        }
        return false;
    }
}
